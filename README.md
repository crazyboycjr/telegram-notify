**Telegram**

These scripts are used to send Telegram messages from a Debian or Ubuntu server.

About how to get *User-ID* in the configuration, see http://www.bernaerts-nicolas.fr/linux/75-debian/351-debian-send-telegram-notification
